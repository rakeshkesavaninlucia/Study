package com.handson.PageObejct;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Homepage 
{
	
   WebDriver driver;
  
	public Homepage(WebDriver driver)
    {

        this.driver = driver;
        PageFactory.initElements(driver, this);
   
    }
	
	@FindBy(xpath="//a/i[@class='fa fa-user fa-2x']")
	WebElement userIcon;
	
	@FindBy(linkText="Log in")
	WebElement logInLink;
	
	@FindBy(id="webpush-custom-prompt-button1")
	WebElement dontAllow;
	
	@FindBy(id="serviceTabA")
	WebElement counSellingServices;
	
	@FindBy(xpath="//div[@id='serviceTab']/div/div/ul/li/a")
	List<WebElement> differentServices;
	 /**

	    * All WebElements are identified by @FindBy annotation

	    */

	public void navigateToUserIcon() throws InterruptedException
	{
		userIcon.click();
		System.out.println("clicked on User Icon");
		Thread.sleep(2000);
	}
	
	public void navigateToLoginPage()
	{
		logInLink.click();
		System.out.println("Clicked on LoginLink");
	}
	
	public void counsellingService() throws InterruptedException
	{
		dontAllow.click();
		Thread.sleep(2000);
		counSellingServices.click();
		
	}
	
	
	public void counsellingServiceSelection(String couselingserviceuser)
	{
		List<WebElement> cousellingselection = differentServices;
		int size = cousellingselection.size();
		System.out.println("size of counseeling services"+size);
		
		for (int i=0;i<size;i++)
		{
			System.out.println("entered into the loop" +i);
			WebElement singlecounseeling = cousellingselection.get(i);
			String currenttext = singlecounseeling.getText();
			if(couselingserviceuser.equals(currenttext))
			{
				singlecounseeling.click();
				System.out.println("clicked the option");
				break;
			}
			else
			{
				System.out.println("ITem not in the list");
			}
			
		}
	}
	
}
